ynn_worship_1 = {
	prestige = 1.0

	allow = { religion = ynn_river_worship }

	sprite = 32
}

ynn_worship_2 = {
	advisor_cost = -0.15

	allow = { religion = ynn_river_worship }

	sprite = 32
}

ynn_worship_3 = {
	monarch_military_power = 1

	allow = { religion = ynn_river_worship }

	sprite = 32
}

ynn_worship_4 = {
	war_exhaustion = -0.03

	allow = { religion = ynn_river_worship }

	sprite = 32
}

ynn_worship_5 = {
	land_morale = 0.10

	allow = { religion = ynn_river_worship }

	sprite = 32
}

ynn_worship_6 = {
	build_cost = -0.10

	allow = { religion = ynn_river_worship }

	sprite = 32
}

ynn_worship_7 = {
	land_attrition = -0.15

	allow = { religion = ynn_river_worship }

	sprite = 32
}

ynn_worship_8 = {
	trade_steering = 0.25

	allow = { religion = ynn_river_worship }

	sprite = 32
}

ynn_worship_9 = {
	monarch_admin_power = 1

	allow = { religion = ynn_river_worship }

	sprite = 32
}

ynn_worship_10 = {
	inflation_reduction = 0.1

	allow = { religion = ynn_river_worship }

	sprite = 32
}

ynn_worship_11 = {
	global_unrest = 1

	allow = { religion = ynn_river_worship }

	sprite = 32
}

ynn_worship_12 = {
	shock_damage = 0.1

	allow = { religion = ynn_river_worship }

	sprite = 32
}

ynn_worship_13 = {
	defensiveness = 0.20

	allow = { religion = ynn_river_worship }

	sprite = 32
}

ynn_worship_14 = {
	monarch_diplomatic_power = 1

	allow = { religion = ynn_river_worship }

	sprite = 32
}

ynn_worship_15 = {
	improve_relation_modifier = 0.25

	allow = { religion = ynn_river_worship }

	sprite = 32
}

ynn_worship_16 = {
	fire_damage = 0.1

	allow = { religion = ynn_river_worship }

	sprite = 32
}

