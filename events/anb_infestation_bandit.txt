namespace = infestation_bandit

# for testing, use event infestation_bandit.0 to force spawn in one of your provinces

# notes on bandits:
# bandit infestations have three sizes, 1, 2, 3 with three being the largest
# bandits can be civilized as racial minority
# bandits can rebel at size 2 or 3, with demands to civilize

# possible flags and province modifiers
# infestation_present - province flag, universal
# infestation_bandit_1, infestation_bandit_2, infestation_bandit_3 for sizes of bandits
# infestation_bandit_rebels - country flag so that we can clean up after a rebellion

# spawn bandit infestation in this province and start event loop
province_event = {
    id = infestation_bandit.0
    title = infestation_bandit.0.t
    desc = infestation_bandit.0.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = yes
    is_triggered_only = yes
    
    immediate = {
        hidden_effect = {
            set_province_flag = infestation_present # important; or event loop will exit
        }
    }
    
    option = {
        ai_chance = { factor = 100 }
        add_permanent_province_modifier  = {
            name = infestation_bandit_1
            duration = -1  
            desc = infestation_bandit_1_tooltip
        }
        province_event = { id = infestation_bandit.100 days = 1 } # spawn response event
    }
    after = {
        # first event in half a year, ish; start event loop
        #province_event = { id = infestation_bandit.1 days = 100 random = 100 }
        set_province_flag = infestation_pulse_flag #we don't want to start pulsing yet
        add_province_triggered_modifier = infestation_bandit_monthly_pulse # we don't want this to start pulsing yet
    }
}

# random event dispatcher, on a loop until infestation_present is removed
province_event = {
    id = infestation_bandit.1
    title = infestation_bandit.1.t
    desc = infestation_bandit.1.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = yes
    is_triggered_only = yes
    

    trigger = {
        has_province_flag = infestation_present
        OR = { 
            has_province_modifier = infestation_bandit_1
            has_province_modifier = infestation_bandit_2
            has_province_modifier = infestation_bandit_3
        }
    }
	
	option = {
		hidden_effect = {
			random_list = {
				1 = { # vanish
					trigger = { has_province_modifier = infestation_bandit_1 }
					province_event = { id = infestation_bandit.101 }
				}
				3 = {
					trigger = {
						owner = { num_of_cities = 2 } # you have something to migrate to
						any_neighbor_province = { 
							owned_by = ROOT
							NOT = { has_province_flag = infestation_present }
						}
					}
					province_event = { id = infestation_bandit.110 }
				}
				1 = { # migrate across border
					trigger = {
						any_neighbor_province = { 
							NOT = { owned_by = ROOT }
							NOT = { has_province_flag = infestation_present }
						}
					}
					province_event = { id = infestation_bandit.111 }
				}
				1 = { # banish (migrate to random neighbour)
					modifier = {
						factor = 3 # more likely if generous quest rewards/adverturer nation
						OR = {
							owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards }
							owner = { has_adventurer_reform = yes }
						}
					}
					province_event = { id = infestation_bandit.115 }
				}
				4 = { # Spreads internally
					trigger = { 
						OR = { 
							has_province_modifier = infestation_bandit_2 
							has_province_modifier = infestation_bandit_3
						}
					}
					province_event = { id = infestation_bandit.120 }
				}
				2 = { # Spreads across borders
					trigger = { 
						OR = { 
							has_province_modifier = infestation_bandit_2 
							has_province_modifier = infestation_bandit_3
						}
					}
					province_event = { id = infestation_bandit.121 }
				}
				4 = { # grows
					trigger = { 
						OR = { 
							has_province_modifier = infestation_bandit_1 
							has_province_modifier = infestation_bandit_2
						}
					}
					province_event = { id = infestation_bandit.130 }
				}
				1 = { # shrinks
					trigger = { 
						OR = { 
							has_province_modifier = infestation_bandit_2 
							has_province_modifier = infestation_bandit_3
						}
					}
					province_event = { id = infestation_bandit.131 }
				}
				6 = { # devastation
					province_event = { id = infestation_bandit.140 }
				}
				1 = { # provoke rebellion
					trigger = { 
						OR = { 
							has_province_modifier = infestation_bandit_2 
							has_province_modifier = infestation_bandit_3
						}
					}
					modifier = {
						factor = 5 # more likely if generous quest rewards/adverturer nation
						OR = {
							owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards }
							owner = { has_adventurer_reform = yes }
						}
					}
					province_event = { id = infestation_bandit.146 }
				}
				1 = { # infestation killed -> shrinks/vanishes
					modifier = {
						factor = 5 # more likely if generous quest rewards/adverturer nation
						OR = {
							owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards }
							owner = { has_adventurer_reform = yes }
						}
					}
					province_event = { id = infestation_bandit.150 }
				}
			}
		}
	}
    
    #after = {
    #    # loop this random event dispatcher
    #    # about a year between events
    #    #province_event = { id = infestation_bandit.1 days = 300 random = 100 }
    #}
}


# notification of infestation spawning
province_event = {
    id = infestation_bandit.100
    title = infestation_bandit.100.t
    desc = infestation_bandit.100.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    option = {
        # what's the worst that can happen?
        name = infestation_bandit.100.a
        ai_chance = { factor = 50 }
    }
    option = {
        # grant adventurer's generous rewards to look after it
        name = infestation_bandit.100.b
        ai_chance = { 
            factor = 20 
            modifier = {
                factor = 0
                owner = { is_in_deficit = yes }
            }
        }
        trigger = {
            owner = { has_estate = estate_adventurers }
            owner = { NOT = { has_estate_privilege = estate_adventurers_generous_quest_rewards } }
        }
        owner = { set_estate_privilege = estate_adventurers_generous_quest_rewards }
    }
    option = {
        # the adventurer's are on it
        name = infestation_bandit.100.c
        ai_chance = { factor = 50 }
        trigger = {
            owner = { has_estate = estate_adventurers }
            owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards }
        }
    }
    option = {
        # we are adventurers, deal with it ourselves!
        name = infestation_bandit.100.e
        ai_chance = { factor = 50 }
        trigger = {
            owner = { has_adventurer_reform = yes }
        }
    }
}

# infestation vanishes on its own
province_event = {
    id = infestation_bandit.101
    title = infestation_bandit.101.t
    desc = infestation_bandit.101.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        has_province_flag = infestation_present
        has_province_modifier = infestation_bandit_1
    }
    
    option = {
        # oh, thank goodness
        name = infestation_bandit.101.a
        ai_chance = { factor = 100 }
        remove_province_modifier = infestation_bandit_1
        hidden_effect = { remove_province_triggered_modifier = infestation_bandit_monthly_pulse }
        cleanup_infestation = yes
    }
}

# infestation migrates to adjacent province internally event
province_event = {
    id = infestation_bandit.110
    title = infestation_bandit.110.t
    desc = infestation_bandit.110.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = yes
    is_triggered_only = yes
    
    trigger = {
        owner = { num_of_cities = 2 } # you have something to migrate to
        any_neighbor_province = { 
            owned_by = ROOT
            NOT = { has_province_flag = infestation_present }
            has_influencing_fort = no # forts prevent infestations from spawning if active
        }
    }
    
    immediate = {
        hidden_effect = {
            random_neighbor_province = {
                limit = {
                    owned_by = ROOT
                    NOT = { has_province_flag = infestation_present }
                    has_influencing_fort = no # forts prevent infestations from spawning if active
                }
                save_event_target_as = infestation_migration_target
            }
            event_target:infestation_migration_target = {
                set_province_flag = infestation_present
            }
            hidden_effect = { remove_province_triggered_modifier = infestation_bandit_monthly_pulse }
            cleanup_infestation = yes
        }
    }
    option = {
        name = infestation_bandit.110.a
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_bandit_3
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_bandit_3
                duration = -1  
                desc = infestation_bandit_3_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_bandit_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_bandit_3
    }    
    option = {
        name = infestation_bandit.110.b
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_bandit_2
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_bandit_2
                duration = -1  
                desc = infestation_bandit_2_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_bandit_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_bandit_2
    }    
    option = {
        name = infestation_bandit.110.c
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_bandit_1
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_bandit_1
                duration = -1  
                desc = infestation_bandit_1_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_bandit_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_bandit_1
    }    
    
    #after = {
    #    # first event in half a year, ish
    #    event_target:infestation_migration_target = {
    #        province_event = { id = infestation_bandit.1 days = 100 random = 100 }
    #    }
    #}
}

# infestation migrates across border to adjacent province event
province_event = {
    id = infestation_bandit.111
    title = infestation_bandit.111.t
    desc = infestation_bandit.111.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        any_neighbor_province = { 
            NOT = { owned_by = ROOT }
            NOT = { has_province_flag = infestation_present }
            has_influencing_fort = no # forts prevent infestations from spawning if active
        }
    }
    
    immediate = {
        hidden_effect = {
            random_neighbor_province = {
                limit = {
                    NOT = { owned_by = ROOT }
                    NOT = { has_province_flag = infestation_present }
                    has_influencing_fort = no # forts prevent infestations from spawning if active
                }
                save_event_target_as = infestation_migration_target
            }
            event_target:infestation_migration_target = {
                set_province_flag = infestation_present
            }
            hidden_effect = { remove_province_triggered_modifier = infestation_bandit_monthly_pulse }
            cleanup_infestation = yes
        }
    }
    
    option = {
        name = infestation_bandit.111.a
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_bandit_3
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_bandit_3
                duration = -1  
                desc = infestation_bandit_3_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_bandit_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_bandit_3
    }
    option = {
        name = infestation_bandit.111.b
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_bandit_2
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_bandit_2
                duration = -1  
                desc = infestation_bandit_2_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_bandit_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_bandit_2
    }
    option = {
        name = infestation_bandit.111.c
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_bandit_1
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_bandit_1
                duration = -1  
                desc = infestation_bandit_1_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_bandit_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_bandit_1
    }
    after = {
        event_target:infestation_migration_target = {
            province_event = { id = infestation_bandit.112 }
        }
    }
}

# infestation migrated/spread across border into this province event
# basically the same as infestation_bandit.100, but with opinion modifier
# and more flavourful descriptions
province_event = {
    id = infestation_bandit.112
    title = infestation_bandit.112.t
    desc = infestation_bandit.112.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    immediate = {
    }
    option = {
        # How could they do this to us!?
        name = infestation_bandit.112.a
        ai_chance = { factor = 50 }
        owner = {
            add_opinion = {
                who = FROM
                modifier = infestation_crossed_border
            }
        }
    }
    option = {
        # grant adventurer's generous rewards to look after it
        name = infestation_bandit.112.b
        ai_chance = { 
            factor = 20 
            modifier = {
                factor = 0
                owner = { is_in_deficit = yes }
            }
        }
        trigger = {
            owner = { has_estate = estate_adventurers }
            owner = { NOT = { has_estate_privilege = estate_adventurers_generous_quest_rewards } }
        }
        owner = { set_estate_privilege = estate_adventurers_generous_quest_rewards }
        owner = {
            add_opinion = {
                who = FROM
                modifier = infestation_crossed_border
            }
        }
    }
    option = {
        # the adventurer's are on it
        name = infestation_bandit.112.c
        ai_chance = { factor = 50 }
        trigger = {
            owner = { has_estate = estate_adventurers }
            owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards }
        }
        owner = {
            add_opinion = {
                who = FROM
                modifier = infestation_crossed_border
            }
        }
    }
    option = {
        # we are adventurers, deal with it ourselves!
        name = infestation_bandit.112.e
        ai_chance = { factor = 50 }
        trigger = {
            owner = { has_adventurer_reform = yes }
        }
        owner = {
            add_opinion = {
                who = FROM
                modifier = infestation_crossed_border
            }
        }
    }
    
    #after = {
    #    # first event in half a year, ish
    #    province_event = { id = infestation_bandit.1 days = 100 random = 100 }
    #}
}

# banished (migrate to a neighbour at random)
province_event = {
    id = infestation_bandit.115
    title = infestation_bandit.115.t
    desc = infestation_bandit.115.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
        
    trigger = {
        owner = {
            any_neighbor_country = {
                any_owned_province = {
                    NOT = { has_province_flag = infestation_present }
                    has_influencing_fort = no # forts prevent infestations from spawning if active
                }
            }
        }
    }
    
    immediate = {
        hidden_effect = {
            owner = {
                random_neighbor_country = {
                    limit = {
                        any_owned_province = {
                            NOT = { has_province_flag = infestation_present }
                            has_influencing_fort = no # active forts prevent infestations from spawning 
                        }
                    }
                    save_event_target_as = infestation_migration_country_target
                }
            }
            event_target:infestation_migration_country_target = {
                random_owned_province = {
                    limit = { 
                        NOT = { has_province_flag = infestation_present } 
                        has_influencing_fort = no # forts prevent infestations from spawning if active
                    }
                    save_event_target_as = infestation_migration_target
                }
            }
            event_target:infestation_migration_target = {
                set_province_flag = infestation_present
            }
            hidden_effect = { remove_province_triggered_modifier = infestation_bandit_monthly_pulse }
            cleanup_infestation = yes
        }
    }
    
    option = {
        name = infestation_bandit.115.a
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_bandit_3
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_bandit_3
                duration = -1  
                desc = infestation_bandit_3_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_bandit_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_bandit_3
    }
    option = {
        name = infestation_bandit.115.b
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_bandit_2
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_bandit_2
                duration = -1  
                desc = infestation_bandit_2_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_bandit_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_bandit_2
    }
    option = {
        name = infestation_bandit.115.c
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_bandit_1
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_bandit_1
                duration = -1  
                desc = infestation_bandit_1_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_bandit_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_bandit_1
    }
    after = {
        event_target:infestation_migration_target = {
            province_event = { id = infestation_bandit.112 }
        }
        if = {
            limit = { owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards } }
            owner = {
                add_estate_influence_modifier = {
                    estate = estate_adventurers
                    desc = handled_infestation
                    duration = 3650
                    influence = 5
                }
            }
        }
    }
}

# infestation spreads to adjacent province internally event
province_event = {
    id = infestation_bandit.120
    title = infestation_bandit.120.t
    desc = infestation_bandit.120.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        owner = { num_of_cities = 2 } # you have something to migrate to
        any_neighbor_province = { 
            owned_by = ROOT
            NOT = { has_province_flag = infestation_present }
            has_influencing_fort = no # forts prevent infestations from spawning if active
        }
        OR = {
            has_province_modifier = infestation_bandit_3
            has_province_modifier = infestation_bandit_2
        }
    }
    immediate = {
        hidden_effect = {
            random_neighbor_province = {
                limit = {
                    owned_by = ROOT
                    NOT = { has_province_flag = infestation_present }
                    has_influencing_fort = no # forts prevent infestations from spawning if active
                }
                save_event_target_as = infestation_spread_target
            }
            event_target:infestation_spread_target = {
                set_province_flag = infestation_present
            }
        }
    }
    option = {
        name = infestation_bandit.120.a
        ai_chance = { factor = 100 }
        goto = infestation_spread_target
        event_target:infestation_spread_target = {
            add_permanent_province_modifier  = {
                name = infestation_bandit_1
                duration = -1  
                desc = infestation_bandit_1_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_bandit_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
    }
    #after = {
    #    # first event in half a year, ish
    #    event_target:infestation_spread_target = {
    #        province_event = { id = infestation_bandit.1 days = 100 random = 100 }
    #    }
    #}
}

# infestation spreads across border to adjacent province event
province_event = {
    id = infestation_bandit.121
    title = infestation_bandit.121.t
    desc = infestation_bandit.121.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        any_neighbor_province = { 
            NOT = { owned_by = ROOT }
            NOT = { has_province_flag = infestation_present }
            has_influencing_fort = no # forts prevent infestations from spawning if active
        }
        OR = {
            has_province_modifier = infestation_bandit_3
            has_province_modifier = infestation_bandit_2
        }
    }
    immediate = {
        hidden_effect = {
            random_neighbor_province = {
                limit = {
                    NOT = { owned_by = ROOT }
                    NOT = { has_province_flag = infestation_present }
                    has_influencing_fort = no # forts prevent infestations from spawning if active
                }
                save_event_target_as = infestation_migration_target
            }
            event_target:infestation_migration_target = {
                set_province_flag = infestation_present
            }
        }
    }
    option = {
        name = infestation_bandit.121.a
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_bandit_1
                duration = -1  
                desc = infestation_bandit_1_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_bandit_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
    }
    after = {
        event_target:infestation_migration_target = {
            province_event = { id = infestation_bandit.112 }
        }
    }
}

# infestation grows in strength
province_event = {
    id = infestation_bandit.130
    title = infestation_bandit.130.t
    desc = infestation_bandit.130.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        has_province_flag = infestation_present
        OR = { 
            has_province_modifier = infestation_bandit_1 
            has_province_modifier = infestation_bandit_2
        }
    }
    option = {
        # should we be worried?
        name = infestation_bandit.130.a
        ai_chance = { factor = 100 }
        if = {
            limit = { has_province_modifier = infestation_bandit_2 }
            hidden_effect = { remove_province_modifier = infestation_bandit_2 }
            add_permanent_province_modifier  = {
                name = infestation_bandit_3
                duration = -1  
                desc = infestation_bandit_3_tooltip
            }
        }
        if = {
            limit = { has_province_modifier = infestation_bandit_1 }
            hidden_effect = { remove_province_modifier = infestation_bandit_1 }
            add_permanent_province_modifier  = {
                name = infestation_bandit_2
                duration = -1  
                desc = infestation_bandit_2_tooltip
            }
        }
    }
}

# infestation shrinks in strength
province_event = {
    id = infestation_bandit.131
    title = infestation_bandit.131.t
    desc = infestation_bandit.131.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        has_province_flag = infestation_present
        OR = { 
            has_province_modifier = infestation_bandit_2 
            has_province_modifier = infestation_bandit_3
        }
    }
    
    option = {
        # should we be worried?
        name = infestation_bandit.131.a
        ai_chance = { factor = 100 }
        if = {
            limit = { has_province_modifier = infestation_bandit_2 }
            hidden_effect = { remove_province_modifier = infestation_bandit_2 }
            add_permanent_province_modifier  = {
                name = infestation_bandit_1
                duration = -1  
                desc = infestation_bandit_1_tooltip
            }
        }
        else_if = {
            limit = { has_province_modifier = infestation_bandit_3 }
            hidden_effect = { remove_province_modifier = infestation_bandit_3 }
            add_permanent_province_modifier  = {
                name = infestation_bandit_2
                duration = -1  
                desc = infestation_bandit_2_tooltip
            }
        }
    }
}

# devastation
province_event = {
    id = infestation_bandit.140
    title = infestation_bandit.140.t
    desc = infestation_bandit.140.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    option = {
        name = infestation_bandit.140.a
        ai_chance = { factor = 20 }
        add_devastation = 5
    }
    option = {
        # crack down hard
        name = infestation_bandit.140.b
        trigger = { 
            OR = { 
                has_province_modifier = infestation_bandit_2 
                has_province_modifier = infestation_bandit_3
            }
        }
        ai_chance = { factor = 80 }
        add_devastation = 10
        province_event = { id = infestation_bandit.131 days = 30 } # shrinks in size
    }
    option = {
        # wipe them out
        name = infestation_bandit.140.c
        trigger = { 
            has_province_modifier = infestation_bandit_1 
        }
        ai_chance = { factor = 80 }
        add_devastation = 15
        province_event = { id = infestation_bandit.101 days = 30 } # vanishes
    }
}

# adventurers provoke rebellion
province_event = {
    id = infestation_bandit.146
    title = infestation_bandit.146.t
    desc = infestation_bandit.146.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
        
    option = {
        # provoke them!
        name = infestation_bandit.146.a
        ai_chance = { factor = 50 }
        set_country_flag = infestation_bandit_rebels
        spawn_rebels = {
            type = infestation_bandit_rebels
            size = 1
        }
    }
    option = {
        # a terrible idea
        name = infestation_bandit.146.b
        ai_chance = { factor = 50 }
    }
	option = {
		#negotiations
		name = infestation_bandit.146.c
		province_event = { id = infestation_bandit.155 days = 7 }
	}
}

# post rebellion cleanup - bandits removed from country
country_event = {
    id = infestation_bandit.148
    title = infestation_bandit.148.t
    desc = infestation_bandit.148.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        has_country_flag = infestation_bandit_rebels
        FROM = {
            tag = REB
        }
        NOT = { has_spawned_rebels = infestation_bandit_rebels }
    }
    option = {
        # bandits utterly defeated
        name = infestation_bandit.148.a
        clr_country_flag = infestation_bandit_rebels
        hidden_effect = {
            every_owned_province = {
                limit = {
                    OR = {
                        has_province_modifier = infestation_bandit_1
                        has_province_modifier = infestation_bandit_2
                        has_province_modifier = infestation_bandit_3
                    }
                }
                remove_province_modifier = infestation_bandit_1
                remove_province_modifier = infestation_bandit_2
                remove_province_modifier = infestation_bandit_3
                hidden_effect = { remove_province_triggered_modifier = infestation_bandit_monthly_pulse }
                cleanup_infestation = yes
            }
        }
    }
}

# kill bandits
province_event = {
    id = infestation_bandit.150
    title = infestation_bandit.150.t
    desc = infestation_bandit.150.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        has_province_flag = infestation_present
    }
    
    option = {
        # great job!
        name = infestation_bandit.150.a
        ai_chance = { factor = 100 }
        if = {
			limit = { 
				has_province_modifier = infestation_bandit_1 
			}
			remove_province_modifier = infestation_bandit_1 
			hidden_effect = { remove_province_triggered_modifier = infestation_bandit_monthly_pulse }
			cleanup_infestation = yes
		}
		else_if = {
			limit = { 
				has_province_modifier = infestation_bandit_2 
			}
			remove_province_modifier = infestation_bandit_2
			add_permanent_province_modifier  = { 
				name = infestation_bandit_1
				duration = -1
			}
		}
		else_if = {
			limit = { 
				has_province_modifier = infestation_bandit_3 
			}
			remove_province_modifier = infestation_bandit_3
			add_permanent_province_modifier  = { 
				name = infestation_bandit_2
				duration = -1
			}
		}
        owner = {
            add_estate_influence_modifier = {
                estate = estate_adventurers
                desc = handled_infestation
                duration = 3650
                influence = 5
            }
        }
    }
}

# infestation successfully negotiated with; some upside with minor downside
province_event = {
    id = infestation_bandit.155
    title = infestation_bandit.155.t
    desc = infestation_bandit.155.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
	
	option = {
		#negotiations
		name = infestation_bandit.155.a
		ai_chance = { factor = 50 }
		if = {
			limit = { 
				has_province_modifier = infestation_bandit_1 
			}
			owner = {
				add_yearly_manpower = 0.1
				add_years_of_income = -0.1
			}
			remove_province_modifier = infestation_bandit_1
			hidden_effect = { remove_province_triggered_modifier = infestation_bandit_monthly_pulse }
			cleanup_infestation = yes
		}
		else_if = {
			limit = { 
				has_province_modifier = infestation_bandit_2 
			}
			owner = {
				add_yearly_manpower = 0.25
				add_years_of_income = -0.25
			}
			remove_province_modifier = infestation_bandit_2
			hidden_effect = { remove_province_triggered_modifier = infestation_bandit_monthly_pulse }
			cleanup_infestation = yes
		}
		else_if = {
			limit = { 
				has_province_modifier = infestation_bandit_3 
			}
			owner = {
				add_yearly_manpower = 0.5
				add_years_of_income = -0.25
			}
			remove_province_modifier = infestation_bandit_3
			hidden_effect = { remove_province_triggered_modifier = infestation_bandit_monthly_pulse }
			cleanup_infestation = yes
		}
	}
	
	option = {
		#end negotiations
		name = infestation_bandit.155.b
		ai_chance = { factor = 50 }
		set_country_flag = infestation_bandit_rebels
        spawn_rebels = {
            type = infestation_bandit_rebels
            size = 1
        }
	}
}

# cleanup event; wipes all trace of this infestation from the province
# this is useful for debugging
province_event = {
    id = infestation_bandit.2000
    title = infestation_bandit.2000.t
    desc = infestation_bandit.2000.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = yes
    is_triggered_only = yes
    
    option = {
        # cleanup; hidden
        name = infestation_bandit.2000.a
        ai_chance = { factor = 100 }
        remove_province_modifier = infestation_bandit_1
        remove_province_modifier = infestation_bandit_2
        remove_province_modifier = infestation_bandit_3
        hidden_effect = { remove_province_triggered_modifier = infestation_bandit_monthly_pulse }
        cleanup_infestation = yes
    }
}